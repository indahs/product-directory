import { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import ReactLoading from 'react-loading'
import axios from 'axios'
import LoginImg from './login-img.png'
import FloatingInput from '../../Components/Form/FloatinInput'
import InvalidFeedback from '../../Components/Form/InvalidFeedback'

export default function NewLogin(){
    let navigate = useNavigate()
    const [value, setValue] = useState({username:"", password:"0lelplR"})
    const [usernameError, setUsernameError] = useState('')
    const [passwordError, setPasswordError] = useState('')
    const [error, setError] = useState('')
    const [loading, setLoading] = useState(false)

    const handleChange = (event) => {
      setValue({
          ...value,
          [event.target.name] : event.target.value
      })
    }

    const handleSubmit = async(ev) => {
        ev.preventDefault()
        setLoading(true)   
        await axios.post('https://dummyjson.com/auth/login', value)
        .then((res) => {
            setLoading(false)
            sessionStorage.setItem("user", JSON.stringify(res.data))
            navigate("/")
        })
        .catch((error) => {
            setLoading(false)
            if(error.response.status === 400){
                if(value.username == ""){
                    setUsernameError('Username tidak boleh kosong')
                }else{
                    setUsernameError('')
                }
                if(value.password == ""){
                    setPasswordError('Password tidak boleh kosong')
                }else{
                    setPasswordError('')
                }
                if(value.username != "" && value.password != ""){
                    setError('Password atau Username salah')
                }else{
                    setError('')
                }
            }
        })
    }
    return(
        <div className="d-flex justify-content-center align-items-center">
            <div className="login-page pages bg-white p-4">
                {loading ? 
                <div className="position-absolute top-50 start-50 translate-middle">
                    <ReactLoading type="balls" color="#03AC0E"/>
                </div>
                :
                <div>
                    <div className="text-center my-4">
                        <img className="mt-3" src={LoginImg}></img>
                        <h2 className="mt-5">Hai, Fren!</h2>
                        <span className="fs-5">Selamat Datang di Aplikasi WoiShop</span>
                    </div>
                    <div className='mt-5'></div>
                    <p className={error ? "error-msg" : "offscreen"} aria-live="assertive">{error}</p>
                    <form onSubmit={handleSubmit} className="needs-validation">
                        <div className="mb-3">
                            <FloatingInput inputError={usernameError} handleChange={handleChange} value={value.username} inputName="username" inputType="text" id="username" placeholder="Username Anda" />
                            <InvalidFeedback inputError={usernameError} feedback="Username" />
                        </div>
                        <div className="mg-3">
                            <FloatingInput inputError={passwordError} handleChange={handleChange} value={value.password} inputName="password" inputType="password" id="password" placeholder="Password Anda" />
                            <InvalidFeedback inputError={usernameError} feedback="Password" />
                        </div>
                        <button type="submit" className="btn btn-success rounded-pill w-100 mt-3 py-3">Masuk</button>
                    </form>
                    <div className="mt-3 text-center regis">
                        <span className="text-secondary">Belum punya akun? Daftar <Link to="/register">Di Sini</Link> </span>
                    </div>
                </div>    
            }
            </div>
        </div>
    )
}