import { Link } from 'react-router-dom'
import { useState } from 'react'
import { User, Mail, Lock, UserMinus, UserPlus } from 'react-feather'
import Header from "../../Components/Header"
import FloatingInput from '../../Components/Form/FloatinInput'

export default function Register(){
    const [values, setValues] = useState({
        name: "", phone: "", referalCode: ""
    })
    const [success, setSuccess] = useState(false)
    const handleChange = (event) => {
        setValues({
            ...values,
            [event.target.name] : event.target.value
        })
    }
    const submitHandle = async (event) => {
        event.preventDefault()
        console.log(values)
        setSuccess(true)
    }
    return(
        <div className="d-flex justify-content-center align-items-center">
            <div className="login-page pages bg-white">
                {success ? 
                    <div className="text-center text-success p-5 mt-5 success-msg bg-white">
                        <img src="https://cdn-icons-png.flaticon.com/512/5610/5610944.png" width="50"></img>
                        <h2 className="mt-3">Berhasil Ditambah</h2>
                        <span className="text-secondary regis">Login <Link to="/login">Di Sini</Link> </span>
                    </div> 
                    : 
                    <div>
                        <Header content="register" title="Daftar Baru" />
                        <form className="px-4 mt-5" onSubmit={submitHandle}>
                            <div className="mb-3">
                                <FloatingInput handleChange={handleChange} value={values.name} inputName="username" inputType="text" id="name" placeholder="Nama Lengkap Anda" />
                            </div>
                            <div className="mb-3">
                                <FloatingInput handleChange={handleChange} value={values.phone} inputName="phone" inputType="text" id="phone" placeholder="Nomor Hp Anda" />
                            </div>
                            <div className="mb-3">
                                <FloatingInput handleChange={handleChange} value={values.referalCode} inputName="referalCode" inputType="text" id="name" placeholder="Kode Referal (Opsional)" />
                            </div>
                            <button type="submit" className="btn btn-success rounded-pill w-100 mt-3 py-3">Masuk</button>
                        </form>
                        <div className="mt-3 text-center regis">
                            <span className="text-secondary">Sudah punya akun? Login <Link to="/login">Di Sini</Link> </span>
                        </div>    
                    </div>
                }
            </div>
        </div>
    )
}