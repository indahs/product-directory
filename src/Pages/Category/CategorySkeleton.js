import Skeleton from 'react-loading-skeleton'

export default function CategorySkeleton({cards}){
    return(
        Array(cards).fill(0).map((_, i) => (
            <div className="col-lg-3 col-3 py-2" key={i}>
                <div className="align-items-center text-center">
                    <div className="">
                        <Skeleton height={50}/>
                    </div>
                    <div className="mt-3">
                        <Skeleton/>
                    </div>
                </div>
            </div>
        ))
    )
}