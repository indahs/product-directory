import { useState, useEffect } from "react";
import CategorySkeleton from "./CategorySkeleton";
import CategoryList from "../../Components/Category/CategoryList";
import Header from "../../Components/Header";
import getCategory from "../../Data/Category";

export default function Category() {
  const [dataCategory, setDataCategory] = useState([]);
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    getCategory()
    .then((data) => {
      setDataCategory(data) 
      setLoading(false)
    })
  });

  return (
    <div className="d-flex justify-content-center align-items-center">
      <div className="pages bg-white">
        <Header content="category" title="Kategory" />
        <div className="row px-4 mt-4">
          <div className="col-lg-3 col-3 d-flex justify-content-center">
            <img src="https://www.superindo.co.id/images/new/logo-superindo.png" width={70} height={70} className="rounded-circle"></img>
          </div>
          <div className="col-lg-9 col-9 text-left">
            <h5>Superindo</h5>
            <p className="text-secondary">Menara Bidakara 2, lantai 19 Jl. Jend. Gatot Soebroto kav. 71-73, Jakarta Selatan</p>
          </div>
        </div>
        <hr className="border border-4"/>
        <div className="row mt-3 px-4 category-link pb-5">
          {loading && 
            <CategorySkeleton cards={12}/>
          }
          {dataCategory.map((category) => (
            <CategoryList category={category} />
          ))}
        </div>
      </div>
    </div>
  );
}
