import { useEffect, useState } from "react";
import axios from "axios";
import CartSkeleton from "./CartSkeleton";
import CartList from "../../Components/Cart/CartList";
import {getCart} from "../../Data/Cart";
import Header from "../../Components/Header";


export default function Cart(){
  const [cart, setCart] = useState([])
  const [loading, setLoading] = useState(true)

  const removeItem = async (id) => {
    axios.delete(`https://dummyjson.com/carts/${id}`)
    .then((res) => res.data)
    .then(() => {
      setCart(cart.filter((item) => {
        return item.id !== id
      }))
    })
  }

  useEffect(() => {
    getCart()
    .then((data) =>{
      setCart(data) 
      setLoading(false)
    })
  }, [])

  return(
    <div className="d-flex justify-content-center align-items-center">
      <div className="cart-page pages bg-white">
        <Header content="cart" title="Keranjang" />
        <div className="px-4">
          {loading && 
            <CartSkeleton cards={6} />}
          {cart.map((item) => (
            <CartList cart={item} RemoveItem={removeItem} />
          ))}
         </div>
      </div>
    </div>
  )
}