import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import ProductOnCart from "../../Components/Cart/ProductOnCart"
import Header from "../../Components/Header"

const AddCart = () => {
  const [product, setProduct] = useState({products: []})
  
  const incrementQty = async (id) => {
    let item = await product.products.find((product) => product.id == id)
    if(item.quantity >= 0){
      item.quantity += 1
      item.total = item.quantity * item.price
      item.discountedPrice = Math.ceil(item.total - (item.total * item.discountPercentage/100))
      let total = 0
      for(let i = 0; i < product.products.length; i++){
        total += product.products[i].total
      }
      let discountedTotal = 0
      for(let i = 0; i < product.products.length; i++){
        discountedTotal += product.products[i].discountedPrice
      }
      let update = {
        products : product.products,
        total : total,
        discountedTotal : Math.ceil(discountedTotal)
      }    
      localStorage.setItem("cart",  JSON.stringify(update))
      setProduct(update)
    }
  }
  
  const decrementQty = async (id) => {
    let item = await product.products.find((product) => product.id == id)
    if(item.quantity > 0){
      item.quantity -= 1
      item.total = item.quantity * item.price
      item.discountedPrice = Math.ceil(item.total - (item.total * item.discountPercentage/100))
      let total = 0
      for(let i = 0; i < product.products.length; i++){
        total += product.products[i].total
      }
      let discountedTotal = 0
      for(let i = 0; i < product.products.length; i++){
        discountedTotal += product.products[i].discountedPrice
      }
      let update = {
        products : product.products,
        total : total,
        discountedTotal : Math.ceil(discountedTotal)
      }    
      localStorage.setItem("cart",  JSON.stringify(update))
      setProduct(update)
    }
  }

  useEffect(() => {
    let newProduct = JSON.parse(localStorage.getItem("cart"))
    setProduct(newProduct)
  }, [])


  return(
    <div className="d-flex justify-content-center">
      <div className="pages bg-white">
        <Header content="cart" title="Keranjang" />
        <div className="d-flex justify-content-between mt-4 px-4">
          <h6>Pesanan</h6>
          <Link to="/" className="text-success text-decoration-none fw-semibold">+ tambah pesanan</Link>
        </div>
        <hr className="my-0"/>
        {product ? 
        <div className="px-4">
          {product.products.map((item) => (
            <ProductOnCart product={item} incrementQty={incrementQty} decrementQty={decrementQty} />
          ))}
          <div className="product-total row py-3">
            <div className="col-lg-9 col-9">
              <h6>Total Harga</h6>
            </div>
            <div className="col-lg-3 col-3">
              <span className="fw-semibold">$ {product.total}</span>
            </div>
            <div className="col-lg-9 col-9">
              <h6>Total Pembayaran</h6>
            </div>
            <div className="col-lg-3 col-3">
              <span className="fw-semibold">$ {product.discountedTotal}</span>
            </div>
          </div>
        </div> : 
        <div></div>
        }
        <div className="px-4">
          <button className="btn btn-success rounded-pill w-100 p-3 mt-2">Buat Pesanan</button>
        </div>
      </div>
    </div>
  )
}

export default AddCart