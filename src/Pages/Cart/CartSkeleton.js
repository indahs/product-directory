import Skeleton from 'react-loading-skeleton'

export default function CartSkeleton({cards}){
    return(
        Array(cards).fill(0).map((_, i) => (
            <div className="mt-2" key={i}>
                <div className="align-items-center text-center">
                    <div className="">
                        <Skeleton height={150}/>
                    </div>
                </div>
            </div>
        ))
    )
}