import { useState, useEffect } from "react"
import { useParams } from "react-router-dom"
import ProductOnCart from "../../Components/Cart/ProductOnCart"
import Header from "../../Components/Header"
import { getCartDetail, updateProductsOnCart } from "../../Data/Cart"

export default function CartDetail() {
  let { cartId } = useParams() 
  const [detailProductsCart, setDetailProductsCart] = useState([])
  const [total, setTotal] = useState(0)
  const [discountedTotal, setDiscountedTotal] = useState(0)

  const incrementQty = async (id) => {
    let item = await detailProductsCart.find((product) => product.id == id)
    if(item.quantity > 0){
      item.quantity += 1
      let update = {
        products : detailProductsCart
      }
      updateProductsOnCart(update, cartId)
      .then((data) => {
        setTotal(data.total)
        setDiscountedTotal(data.discountedTotal)
        console.log(data)
      });
    }
  }
  
  const decrementQty = async (id) => {
    let item = await detailProductsCart.find((product) => product.id == id)
    if(item.quantity > 1){
      item.quantity -= 1
      let update = {
        products : detailProductsCart
      }
      console.log(update)
      updateProductsOnCart(update, cartId)
      .then((data) => {
        setTotal(data.total)
        setDiscountedTotal(data.discountedTotal)
        console.log(data)
      });
    }
  }

  useEffect(() => {
    getCartDetail(cartId)
    .then((data) =>{
      setDetailProductsCart(data.products.sort(function(a,b){
        return a.id - b.id
      }))
      setTotal(data.total)
      setDiscountedTotal(data.discountedTotal)
    })
  }, [])

  return(
    <div className="d-flex justify-content-center align-items-center">
      <div className="cart-detail pages bg-white">
        <Header content="cart" title="Keranjang" />
        <div className="px-4">
          <h6 className="mt-4">Pesanan</h6>
          <hr className="my-0"/>
          {detailProductsCart.map((item) => (
            <ProductOnCart product={item} incrementQty={incrementQty} decrementQty={decrementQty} />
          ))}
          <div className="product-total row py-3">
            <div className="col-lg-9 col-9">
              <h6>Total Harga</h6>
            </div>
            <div className="col-lg-3 col-3">
              <span className="fw-semibold">$ {total}</span>
            </div>
            <div className="col-lg-9 col-9">
              <h6>Total Pembayaran</h6>
            </div>
            <div className="col-lg-3 col-3">
              <span className="fw-semibold">$ {discountedTotal}</span>
            </div>
          </div>
          <button className="btn btn-success rounded-pill w-100 p-3 mt-2">Buat Pesanan</button>
        </div>
      </div>
    </div>
  )
}