import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { ArrowLeft } from "react-feather"
import { Splide, SplideSlide } from '@splidejs/react-splide';
import { getProductDetail, postProductToCart } from "../../Data/Product";
import Skeleton from 'react-loading-skeleton'
import '@splidejs/react-splide/css';

export default function ProductDetail(){
    let navigate = useNavigate()
    let { productId } = useParams()
    const [detailProduct, setDetailProduct] = useState([])
    const [Loading, setLoading] = useState(true)

    const AddToCart = (detailProduct) => {
        setLoading(true)
        const cart = JSON.parse(localStorage.getItem("cart"))
        let user = JSON.parse(sessionStorage.getItem("user"))
        if(cart){
            let productOnCart = {
                id : detailProduct.id,
                quantity : 1
            }
            let newProduct = {
                userId : user.id,
                products: [...cart.products, productOnCart]
            }
            postProductToCart(newProduct)
              .then((data) =>{
                setLoading(false)
                localStorage.setItem("cart", JSON.stringify(data))
                navigate("/cart/addCart")
            })
        }else{
            let productOnCart = {
                id : detailProduct.id,
                quantity : 1
            }            
            let newCart = {
                userId : user.id,
                products: [productOnCart]
            }
            postProductToCart(newCart)
              .then((data) =>{
                localStorage.setItem("cart", JSON.stringify(data))
                navigate("/cart/addCart")
              })
        }
    }

    useEffect(() => {
        getProductDetail(productId)
        .then(data => {
            setDetailProduct(data)
            setLoading(false)
        })
    }, [])

    return(
        <div className="d-flex justify-content-center align-items-center">
            <div className="product-detail pages bg-white p-4">
                {Loading ? 
                <div className="row">
                    <a className="back-button" onClick={() => navigate(-1)}><ArrowLeft size={20} color="#03AC0E" /></a>
                    <Skeleton height={230} className="mt-4" />
                    <Skeleton className="mt-3"/>
                    <Skeleton className="mb-3"/>
                    <Skeleton count={4}/>
                </div> 
                : 
                <div className="">
                    <a className="back-button" onClick={() => navigate(-1)}><ArrowLeft size={20} color="#03AC0E" /></a>
                    <div className="text-center mt-lg-0 mt-4">
                        <Splide options={ { rewind: true, gap   : '1rem', } } aria-label="My Favorite Images">
                            {detailProduct.images.map((img) => (
                                <SplideSlide key={img}>
                                    <img src={img} alt="Image 1"/>
                                </SplideSlide>
                            ))}
                        </Splide>
                    </div>
                    <div className="mt-5">
                        <span className="badge bg-secondary fw-semibold">{detailProduct.category}</span>
                        <h6 className="my-3 fw-semibold fs-5">{detailProduct.title}</h6>
                        <h6 className="fw-semibold fs-5">$ {detailProduct.price}</h6>
                        <hr className="my-4"></hr>
                        <h6 className="fw-semibold fs-5">Deskripsi Produk</h6>
                        <p className="text-secondary">{detailProduct.description}</p>
                    </div>
                    <button className="btn btn-success mt-4 p-3 text-sm w-100 rounded-pill" onClick={() => AddToCart(detailProduct)}>Tambah Pesanan</button>
                </div>
                }                    
            </div>
        </div>
    )
}
