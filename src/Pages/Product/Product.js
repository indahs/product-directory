import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { getProduct, postProductToCart } from "../../Data/Product";
import ProductSkeleton from "./ProductSkeleton";
import ProductList from "../../Components/Product/ProductList";
import Header from "../../Components/Header";

export default function Product() {
  let navigate = useNavigate()
  let { category } = useParams();
  const [dataProduct, setDataProduct] = useState([])
  const [loading, setLoading] = useState(true)

  const AddToCart = (product) => {
    const cart = JSON.parse(localStorage.getItem("cart"))
    const user = JSON.parse(sessionStorage.getItem("user"))
    if(cart){
      let productOnCart = {
        id : product.id,
        quantity : 1
      }
      let newProduct = {
        userId : user.id,
        products: [...cart.products, productOnCart]
      }
      postProductToCart(newProduct)
      .then((data) =>{
        localStorage.setItem("cart", JSON.stringify(data))
        navigate("/cart/addCart")
      })
    }else{
      let productOnCart = {
        id : product.id,
        quantity : 1
      }
      let newCart = {
        userId : user.id,
        products : [productOnCart]
      }
      postProductToCart(newCart)
      .then((data) =>{
        localStorage.setItem("cart", JSON.stringify(data))
        navigate("/cart/addCart")
      })
    }
  }

  useEffect(() => {
    getProduct(category)
    .then((data) => {
      setDataProduct(data) 
      setLoading(false)
    })
  }, [])

  return (
    <div className="d-flex justify-content-center align-items-center">
      <div className="pages product bg-white">
        <Header content="product" title={category} />
        <div className="row mt-3 px-4">
          {loading && 
            <ProductSkeleton cards={8}/>
          }
          {dataProduct.map(item => (
            <ProductList key={item.id} product={item} AddToCart={AddToCart} />
          ))}
        </div>
      </div>
    </div>
  );
}