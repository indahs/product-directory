import Skeleton from 'react-loading-skeleton'

export default function ProductSkeleton({cards}){
    return(
        Array(cards).fill(0).map((_, i) => (
            <div className="col-lg-4 col-4  p-3" style={{height: "150px"}} key={i}>
                <div className="text-center">
                    <Skeleton height={70} />
                </div>
                <div className="mt-2">
                    <Skeleton count={2} />
                </div>
            </div>
        ))
    )
}