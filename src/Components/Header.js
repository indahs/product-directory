import { Link, useNavigate } from "react-router-dom";
import { ShoppingCart, ArrowLeft } from "react-feather";

const Header = ({content, title}) => {
  let navigate = useNavigate()

  return ( 
    <div>
      {content == "category" ? 
        <div className="">
          <div className="header px-4 py-3 d-flex justify-content-between">
            <h6 className="fs-5 fw-semibold">{title}</h6>
            <Link to="/cart"><ShoppingCart size={16}/></Link>
          </div>
        </div>
        : 
        <div className="d-flex header px-4 py-3 mb-2">
          <a className="back-button" onClick={() => navigate(-1)}><ArrowLeft size={20} color="#03AC0E"/></a>
          <h6 className="mx-3">{title}</h6>
        </div>
      }
    </div>
   );
}
 
export default Header;