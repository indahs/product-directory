import { Link } from "react-router-dom";
import Logo from './woishop.png'

const CartList = ({cart, RemoveItem}) => {
  return (
    <div className="mt-2" key={cart.id}>
      <div className="card bg-light">
        <div className="card-body">
          <div className="d-flex justify-content-between">
            <img src={Logo}></img>
            <button className="btn btn-danger btn-sm rounded" onClick={() => RemoveItem(cart.id)}>
              Del
            </button>
          </div>
          <Link to={`/cart/${cart.id}`} className="text-decoration-none">
            <hr />
            <span className="text-secondary">Total pesanan {cart.totalProducts} produk</span>
            <h5 className="mt-2 fs-5 fw-semibold">$ {cart.total}</h5>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CartList;
