import { MoreHorizontal, Plus, Minus } from "react-feather";

const ProductOnCart = ({product, decrementQty, incrementQty}) => {
  return (
    <div className="product-list row py-2" key={product.id}>
      <div className="col-lg-2 col-2 text-center">
        <MoreHorizontal size={24} color="#03AC0E"/>
      </div>
      <div className="col-lg-6 col-6">
        <span>{product.title}</span>
        <h6 className="mt-2">$ {product.price}</h6>
      </div>
      <div className="col-lg-4 col-4 d-flex align-items-center justify-content-center">
        <div>
          <button className={`btn btn-sm rounded-circle ${product.quantity === 1 ? `btn-outline-secondary disabled` : `btn-outline-success`}`} onClick={() => decrementQty(product.id)}>
            <Minus size={9} />
          </button>
          <h6 className="d-inline mx-2">{product.quantity}</h6>
          <button className="btn btn-outline-success btn-sm rounded-circle" onClick={() => incrementQty(product.id)} >
            <Plus size={9} />
          </button>
        </div>
      </div>
    </div>
  );
};

export default ProductOnCart;
