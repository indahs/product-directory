export default function InvalidFeedback({inputError, feedback}){
  return(
    <div className={inputError ? "invalid-feedback d-block text-center" : "d-none"}>
      {feedback} tidak boleh kosong
    </div>
  )
}