import { Lock, Phone, User, UserPlus,  } from "react-feather"

export default function FloatingInput({inputError, handleChange, value, inputName, inputType, placeholder, id}){
  let icon   
  
  if(inputName == "username" ){
      icon = <User className="mx-3" size={16}/>
    }else if(inputName == "password"){
      icon = <Lock className="mx-3" size={16}/>
    }else if(inputName == "phone"){
      icon = <Phone className="mx-3" size={16}/>
    }else if(inputName == "referalCode"){
      icon = <UserPlus className="mx-3" size={16}/>
    }
  
  return(
    <div className={`input-group ${inputError ? `invalid-input` : ``}`}>
      <span className="input-group-text">{icon}</span>
      <div className="form-floating has-validation">
          <input type={inputType} className="form-control" id={id} placeholder={placeholder} name={inputName} onChange={handleChange} value={value}/>
          <label for={id}>{placeholder}</label>                                
      </div>
    </div>
  )
}