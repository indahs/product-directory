import { Link } from "react-router-dom";
import Image from "./category-img.png"

const CategoryList = ({category}) => {
  return (
    <Link to={`/${category}`} className="col-lg-3 col-3 py-2 category-item text-decoration-none" >
      <div className="align-items-center text-center" key={category}>
        <div className="">
          <img src={Image} height="56" alt={category}/>
        </div>
        <div className="mt-3">
          <span className="text-capitalize">{category}</span>
        </div>
      </div>
    </Link>
  );
};

export default CategoryList;
