import { Link } from "react-router-dom";

const ProductList = ({product, AddToCart}) => {
  return (
    <div className="col-lg-4 col-4 p-3">
      <Link to={`/category/product/${product.id}`} className="text-decoration-none">
        <div className="d-flex justify-content-center align-items-center border rounded-4" style={{ height: "100px" }}>
          <img src={product.thumbnail} alt={product.title}/>
        </div>
        <div className="mt-2" style={{ height: "60px" }}>
          <span className="fw-normal">{product.title}</span>
        </div>
      </Link>
      <div className="d-grid gap-2 mt-4">
        <h6 className="mt-2">$ {product.price}</h6>
        <button className="p-2 d-block btn btn-outline-success btn-sm rounded-pill fw-semibold align-bottom" onClick={() => AddToCart(product)}>
          Tambah
        </button>
      </div>
    </div>
  );
};

export default ProductList;
