import axios from "axios"

const getProduct = (category) => {
  return axios.get(`https://dummyjson.com/products/category/${category}`)
  .then((res) => res.data.products)
}

const getProductDetail = async(productId) => {
  return await axios.get(`https://dummyjson.com/products/${productId}`)
  .then((res) => res.data)
}

const postProductToCart = async(product) => {
  return await axios.post('https://dummyjson.com/carts/add', product)
  .then((res) => res.data)
}



export {getProduct, getProductDetail, postProductToCart}