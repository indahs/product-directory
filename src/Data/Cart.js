import axios from "axios"

const getCart = () => {
  return axios.get('https://dummyjson.com/carts')
  .then((res) => res.data.carts)
}

const getCartDetail = (cartId) => {
  return axios.get(`https://dummyjson.com/carts/${cartId}`)
  .then((res) => res.data)
}

const updateProductsOnCart = (products, cartId) => {
  return axios.put(`https://dummyjson.com/carts/${cartId}`, products)
  .then((res) => res.data)
}

export {getCart, getCartDetail, updateProductsOnCart}