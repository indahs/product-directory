import axios from "axios";

const getCategory = () => {
  return axios.get("https://dummyjson.com/products/categories")
  .then((res) => res.data)
}

export default getCategory