import { Routes, Route } from 'react-router-dom';
import NewLogin from '../Pages/Auth/New-Login'
import Register from '../Pages/Auth/Register';
import Product from '../Pages/Product/Product'
import Category from '../Pages/Category/Category';
import ProductDetail from '../Pages/Product/ProductDetail';
import Cart from '../Pages/Cart/Cart';
import CartDetail from '../Pages/Cart/CartDetail';
import AddCart from '../Pages/Cart/AddCart';
import PrivateRoutes from '../State/PrivateRoutes';

export default function AllRoute(){
  return(
    <Routes>
      <Route element={<PrivateRoutes />}>
          <Route path="/" element={<Category />}></Route>
          <Route path="/:category" element={<Product />}></Route>
          <Route path="/category/product/:productId" element={<ProductDetail />}></Route>
          <Route path="/cart" element={<Cart/>}></Route>
          <Route path="/cart/:cartId" element={<CartDetail/>}></Route>
          <Route path="/cart/addCart" element={<AddCart/>}></Route>
      </Route>
      <Route path="/login" element={<NewLogin />}></Route>
      <Route path="/register" element={<Register />}></Route>
    </Routes>
  )
}