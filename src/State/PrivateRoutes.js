import { Outlet, Navigate } from 'react-router-dom'

const PrivateRoutes = () => {
    let auth = {'user': sessionStorage.getItem("user")}
    return(
        auth.user ? <Outlet/> : <Navigate to="/login"/>
    )
}

export default PrivateRoutes