import { BrowserRouter } from 'react-router-dom';
import AllRoute from './State/AllRoute';
import './App.css';
import 'react-loading-skeleton/dist/skeleton.css'

export default function App(){
  return(
    <BrowserRouter>
        <AllRoute />
    </BrowserRouter>
  )
}
